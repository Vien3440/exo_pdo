<?php

class GameEngine {

private $listeCombatens;

    public function __construct()
    {
        $this->listeCombatens = array();
    }

    public function  addCombattant(Personnage $personnage)
    {
        array_push($this->listeCombatens,$personnage);
        return $this;
    }

    public function start()
    {   
        while (count($this->listeCombatens) != 1) {
            $this->tourDeJeu();
        }
        echo( $this->listeCombatens[0]->getNom() . " est le dernié debout" );
    }

    public function getId()
    {
        $idPersonnage = array_rand($this->listeCombatens, 1);
        return $idPersonnage;
    }

    private function tourDeJeu()
    { //qui effectue les actions de chaque tour
        foreach ($this->listeCombatens as $combaten) {

            $idCible = $this->getId();
            $personnageCible = $this->getJoueur($idCible);

            $combaten->attaquer($personnageCible);
            if ($personnageCible->mort()) {// si la méthode mort du personnage retourne true 
                $this->nettoyerMort($idCible);// alors je déclanche la méthode nettoyerMort 
            }
        }
        return;
    }

    private function  getJoueur($id){ //qui retourne un joueur aléatoirement
        $personnage = $this->listeCombatens[$id];
        return $personnage;
    }

    private function nettoyerMort($id){
        $nouvelleListe = array();
        for ($i=0; $i < count($this->listeCombatens); $i++) { 
            if ($i != $id){
                array_push($nouvelleListe, $this->listeCombatens[$i]);
            }
        }      
        $this->listeCombatens = $nouvelleListe;
    }

}