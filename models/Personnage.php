<?php
    class Personnage {

        protected $force;
        protected $pv;
        protected $endurance;
        protected $nom;


        public function attaquer(Personnage $cible)
        {
            $ciblePv = $cible->pv;
            $ciblePv = $ciblePv - $this->force;
            $cible->pv = $ciblePv;
            echo "<br>" . " Avec une force de " . $this->force . " " .
                $this->nom . " attaque " . $cible->nom . " il lui reste " . $ciblePv . " Pv <br>";
        }


        public function mort() : bool
        {
            $flag = false;
            if ($this->pv <= 0) {
                $flag = true;
            }
            return $flag;
        }


        public function getNom()
        {
            return $this->nom;
        }

    }
?>